# python3

# !pip install chess

import chess

pieces = ['K', 'Q', 'R', 'B', 'N']

moves = [
	'e4',
	'e5',
	'h4',
	'h6',
	'?h3',
	'?h7',
	'd3',
	'f6',
	'?h5+',
	'?e7',
	'a4',
	'd5',
	'?g3',
	'?f5',
	'?xg7+',
	'?xg7',
	'?c3',
	'?f8',
	'?f3',
	'?g6',
	'b3',
	'c6',
	'?d2',
	'?a6',
	'?a3+',
	'?e7',
	'?b2',
	'?d6',
	'?e2',
	'?b4',
	'?a3',
	'?c5',
	'?e3',
	'?c8',
	'?c1',
	'?a2+',
	'?xa2',
	'?e7',
	'?f3',
	'?f5',
	'?xe5',
	'?d6',
	'?xg6+',
	'?f7',
	'?h5',
	'?e6',
	'?d1',
	'?h8',
	'?b2',
	'?a3',
	'?f4+',
	'?e5',
	'?c1',
	'?xc1+',
	'?xc1',
	'?hc7',
	'?a1',
	'?g8',
	'?b2',
	'?g7',
	'?e2',
	'?d4',
	'?b5+',
	'?c5',
	'?d2',
	'?xh4',
	'exd5',
	'cxb5',
	'axb5',
	'b6',
	'?e6+',
	'?xd5',
	'?e1',
	'?e8',
	'Qd4'
]

def get_candidate_moves(index):
    move = moves[index]
    if move[0] == '?':
        candidate_moves = [piece + move[1:] for piece in pieces]
    else:
        candidate_moves = [move]
    return candidate_moves

def get_legal_moves(board, candidates):
    legal_moves = []
    for move in candidates:
        try:
            legal_moves.append(board.parse_san(move))
        except ValueError as e:
            pass
            # print(str(e))
    return legal_moves

boards = [ [chess.Board()] ]

def run(index):
    if index < len(moves):
        new_boards = []

        current_boards = boards[index]

        candidate_moves = get_candidate_moves(index)

        for current_board in current_boards:
            legal_moves = get_legal_moves(current_board, candidate_moves)

            for move in legal_moves:
                new_board = chess.Board(current_board.fen())
                new_board.push(move)
                new_boards.append(new_board)


        boards.append(new_boards)

        print("depth: {}".format(index))
        run(index + 1)

    else:
        print("done")

game_over_boards = []

for board in boards[-1]:
    if board.is_game_over():
        game_over_boards.append(board.fen())

for fen in game_over_boards:
    print(fen)
